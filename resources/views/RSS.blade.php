<!doctype html>
<html lang="sk">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>RSS Reader</title>
  <!--  <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
        <link rel="stylesheet" href="{{ secure_asset('js/scripts.js') }}" >
-->
    <link rel="stylesheet" href="{{ secure_asset('css/styles.css') }}" >
    <script src="{{secure_asset('js/scripts.js')}}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
<nav class="navbar navbar-inverse">
<div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">RSS Reader</a>
    </div>

    <form class="navbar-form navbar-left" action="/" method="get">
        <div class="form-group">
            @if(empty($URL))
                <input type="text" class="form-control" placeholder="RSSLink" name="RSSLink">
                @else
                <input type="text" class="form-control" placeholder="{{$URL}}" value="{{$URL}}" name="RSSLink">
                @endif

        </div>
        <button type="submit" class="btn btn-default">Load / Refresh</button>
    </form>
</div>
</nav>

@if($feed)
    <div class="feedName">
        <h1>{{ $feed->get_title() }}</h1>
    </div>
@endif



<div class="sort">
    @if(empty($URL))
        <div class="Title">
            <h2 style="color: black; text-align: center; position:relative; bottom:23px; font-weight: bold">No RSS Loaded</h2>
        </div>
    @else
        <button class="btn btn-default" onclick="sort_by_date_old()" id="up">Sort Newest to Oldest</button>
        <button class="btn btn-default" onclick="sort_by_date_new()" id="down">Sort Oldest to Newest</button>
    @endif
</div>

<div class="feedContainer" id="list">





    @foreach($feed_items as $item)


    <div class="feedItem" id="{{$item->get_date('YmdHi')}}">
        <div class="Title">
            <h3><a target="_blank" href="{{ $item->get_link() }}">{{ $item->get_title() }}</a></h3>
        </div>

        <div class="desc">
            @if($item->get_description())
            <p>{{ $item->get_description() }} </p>
                @else
                <p>No description.</p>
            @endif
        </div>

        @foreach($item->get_enclosures() as $image)
            @if($image->get_link() != null)
                <img src="{{url($image->get_link())}}" alt="{{$item->get_title()}}">
            @else
                <p>Obrázok nie je k dispozícii</p>
            @endif
            @break
        @endforeach


        @if($item->get_date())
        <div class="date">
            <p>{{ $item->get_date() }}</p>
        </div>
            @else
            <p class="date">No date info.</p>
        @endif

    </div>


        @endforeach

</div>

<!--<script src="{{URL::asset('js/scripts.js')}}"></script> -->

</body>
</html>
