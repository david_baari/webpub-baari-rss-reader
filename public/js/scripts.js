
function sort_by_date_new() {
    let main = document.getElementById('list');

    [].map.call(main.children, Object).sort(function(a, b) {
        return  +a.id.match(/\d+/) - +b.id.match(/\d+/);
    }).forEach(function(elem) {
        main.appendChild(elem);
    });
}

function sort_by_date_old() {
    let main = document.getElementById('list');

    [].map.call(main.children, Object).sort(function(a, b) {
        return  +b.id.match(/\d+/) - +a.id.match(/\d+/);
    }).forEach(function(elem) {
        main.appendChild(elem);
    });
}




