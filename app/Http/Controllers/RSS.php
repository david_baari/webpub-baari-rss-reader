<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Vedmant\FeedReader\Facades\FeedReader;

class RSS extends Controller
{
    public function feedit(Request $request){
       $link = $request->input('RSSLink');
       $feed = FeedReader::read($link);
       $feed->strip_htmltags(array_merge($feed->strip_htmltags, array('h1', 'a', 'img')));
       $feeds = $feed->get_items();
     //  dd($feed, $feeds);
        return view('RSS', ['feed_items' => $feeds, 'feed' => $feed, 'URL' => $link]);
    }
}
// http://rss.cnn.com/rss/edition_meast.rss
